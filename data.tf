// data source to get the secret

data "aws_secretsmanager_secret" "db_credentials" {
  name = "hrz/appmod/poets/rds/credentials"
}

data "aws_secretsmanager_secret_version" "current" {
  secret_id = data.aws_secretsmanager_secret.db_credentials.id
}

locals {
  db_credentials = jsondecode(data.aws_secretsmanager_secret_version.current.secret_string)
}


// data source to get the existing db instance
data "aws_db_instance" "my_existing_db" {
  db_instance_identifier = "preprod-poets-rds"
}
