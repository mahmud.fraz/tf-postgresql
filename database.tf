# Dynamic creation of databases
resource "postgresql_database" "db" {
  for_each = toset(var.databases)

  name = each.value
}
