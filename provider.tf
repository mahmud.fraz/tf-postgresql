provider "aws" {
  region = "eu-west-2"
}

terraform {
  required_providers {
    postgresql = {
      source = "cyrilgdn/postgresql"
      version = "1.21.0"
    }
  }
}

provider "postgresql" {
  host            = data.aws_db_instance.my_existing_db.address
  port            = 5432
  database        = "postgres"
  username        = local.db_credentials["db_instance_username"]
  password        = local.db_credentials["db_instance_password"]
  sslmode         = "require"
}
