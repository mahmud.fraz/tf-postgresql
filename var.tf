variable "databases" {
  type        = list(string)
  description = "List of database names to create"
  default    = ["poets-addons"]
}
